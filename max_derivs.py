import numpy as np
import matplotlib.pyplot as plt

dat = np.loadtxt("max_derivs.out")
step = dat[:, 0]
T = dat[:, 1]
pi = dat[:, 2]
p = dat[:, 3]
du = dat[:, 4]


fig, axs = plt.subplots(1, 1, sharex=True, figsize=(10,10) )

plt.xscale("log")

plt.xlabel("T")

axs.scatter(T, np.log(p), s=0.1)
axs.set_ylabel("k(max dfdt)")

fig.show()
fig.savefig("max_derivs_all.png", dpi=300)



fig, axs = plt.subplots(3, 1, sharex=True, figsize=(10,10) )

plt.xscale("log")

plt.xlim(500, 520)
plt.xlabel("T")

axs[0].scatter(T, step, s=0.1)
axs[0].set_ylim(14000,20000)
axs[0].set_ylabel("step")

axs[1].scatter(T, np.log(p), s=0.1)
axs[1].set_ylim(3.8,4.2)
axs[1].set_ylabel("k(max dfdt)")

axs[2].scatter(T, du, s=0.1)
axs[2].set_ylim(-100,10000)
axs[2].set_ylabel("max dfdt")

fig.show()
fig.savefig("max_derivs.png", dpi=300)
