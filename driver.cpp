// driver for WDMFast integrator

#include "data_array.hpp"
#include "equations.hpp"
#include "errorchecker.hpp"
#include "stateprinter.hpp"
#include <iostream>

#include <boost/numeric/odeint.hpp> // boost ode integrator, make sure to include boost directory in paths
#include <boost/property_tree/json_parser.hpp> // boost .json file reader
#include <boost/property_tree/ptree.hpp>       // boost property tree

namespace odi = boost::numeric::odeint;
namespace pt = boost::property_tree;

/* Main Driver */
int main() {
    // read in parameters from params.json file using boost json parser
    pt::ptree tree;
    pt::read_json("params.json", tree);

    // define system of equations: inputs parameters and builds data splines
    equations system(tree);

    // Define the state printer, that prints the results to ostream
    // And the current derivatives to derivs_file
    std::string distribs_file =
        tree.get<std::string>("output.distribs", "distribs.out");
    std::ofstream os1(distribs_file);
    std::string derivs_file =
        tree.get<std::string>("output.derivs", "derivs.out");
    std::ofstream os2(derivs_file);
    std::ofstream os3("max_derivs.out");
    stateprinter state_printer(system, os1, os2, os3);

    // initial state
    state_type u = system.uinitial();

    // initialise error control for Runge-Kutta
    double abs_err = tree.get<double>("stepper.abs_err", 1e-6);
    double rel_err = tree.get<double>("stepper.rel_err", 1e-6);
    double mode_coeff = tree.get<double>("stepper.mode_coeff", 1);
    double a_x = 1.0, a_dxdt = 1.0;

    typedef odi::runge_kutta_cash_karp54<state_type> error_stepper_type;

    // define the Runge-Kutta stepper using CashKarp54 step and with custom
    // error checking control
    odi::controlled_runge_kutta<error_stepper_type, errorchecker> stepper(
        errorchecker(abs_err, rel_err, a_x, a_dxdt, mode_coeff));

    // perform integral
    double dx0 = -0.01; // initial step (step size is adaptive after 1st step)
    odi::integrate_adaptive(stepper, system, u, log10(system.Tb),
                            log10(system.Te), dx0, state_printer);

    double abundance = system.abund(u);
    std::cout << "Omegah2 = " << abundance << std::endl;

    // std::system("pause");
    return 0;
}
