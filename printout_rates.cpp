// driver for WDMFast integrator

#include "data_array.hpp"
#include "equations.hpp"
#include <iostream>
#include <stdio.h>

#include <boost/property_tree/json_parser.hpp> // boost .json file reader
#include <boost/property_tree/ptree.hpp>       // boost property tree

namespace pt = boost::property_tree;

/* Main Driver */
int main(int argc, char* argv[]) {
    // read in parameters from params.json file using boost json parser
    pt::ptree tree;
    pt::read_json("params.json", tree);

    // define system of equations: inputs parameters and builds data splines
    equations system(tree);

    double T = 1000;
    if (argc>1) {
	sscanf(argv[1], "%lg", &T); 
    }
    std::vector<double> pT;
    for (int i=6; i<=120; i++) {
	pT.push_back(i*0.1);
    }

    system.method_rates = 2; // Abazajan
    system.buildsplines();
    std::vector<double> rates_abazajan;
    for (auto it = pT.begin(); it != pT.end(); it++) {
	rates_abazajan.push_back(
	    system.totalrates_x(T, *it * T)
	    );
    }

    system.method_rates = 1; // Laine
    system.buildsplines();
    std::vector<double> rates_laine;
    for (auto it = pT.begin(); it != pT.end(); it++) {
	rates_laine.push_back(
	    system.totalrates_x(T, *it * T)
	    );
    }

    for (size_t i = 0; i<pT.size(); i++) {
	std::cout << pT[i] << " " << rates_abazajan[i] << " " << rates_laine[i] << std::endl;
    }
    
    
    // std::system("pause");
    return 0;
}
