import json
import numpy as np
import matplotlib.pyplot as plt

with open("params.json") as f:
    params = json.load(f)

theta = params["sterile"]["theta"]
sin22theta = np.sin(2*theta)**2
L0 = params["L0"]

dat = np.genfromtxt("distribs.out", skip_header=1)
T = dat[1, 0]
t = dat[1, 1]
N = dat[1, 2]
L = dat[1, 3]
p = dat[0, 4:]
ff = dat[1, 4:]

n = np.size(p)//2
# Take only one set of momenta, and red shift to physical
p = p[0:n] / np.exp(N)
f = ff[0:n]
fbar = ff[n:n+n]

fig, axs = plt.subplots(1, 1, sharex=True, figsize=(10,10) )

# plt.xscale("log")

plt.xlabel("p")

color = 'tab:red'
axs.plot(p, p*p*f, color=color, label="p^2*f(p)")
axs.set_ylabel("p^2*f(p)")
handles, labels = axs.get_legend_handles_labels()


ax2 = axs.twinx()

color = 'tab:blue'
ax2.plot(p, p*p*fbar, color=color, label="p^2*fbar(p)")
ax2.set_ylabel("p^2*fbar(p)")
handles2, labels2 = ax2.get_legend_handles_labels()

plt.legend(loc="upper right", handles=handles+handles2, labels=labels+labels2)

plt.title("sin^2(2theta)={s}, L0={l}".format(s=sin22theta, l=L0))

fig.show()
fig.savefig("distribs.pdf", dpi=300)

