#include "equations.hpp"
// #include <fstream>
// #include <iomanip>

/* initialise data splines from data files */
// using data_array.h functions to read data and create splines to be stored in
// equations class for use in evolution equations
void equations::buildsplines() {
    // gstar and hstar
    data_array gstardata("SMgstar.dat", 3);
    gstarspline = gstardata.dataspline(1);
    hstarspline = gstardata.dataspline(2);
    // strong fluid susceptibilities
    data_array strongfluiddata("ChiTable_alltemp.dat", 4);
    chi2Bspline = strongfluiddata.dataspline(1);
    chi2Qspline = strongfluiddata.dataspline(2);
    chi11QBspline = strongfluiddata.dataspline(3);

    // OLD ORIGINAL SUSCEPTIBILITIES
    // particle number susceptibilities: Abazajan
    // - Stefan-Boltzmann free particle approximation
    data_array particlesusceptibilitesdata("ParticleChiTable.dat", 2);
    chiPspline = particlesusceptibilitesdata.dataspline(1);

    // Laine's code susceptibilities
    data_array electonsusceptibilitesdata("ElectronSusTable.dat", 2);
    chiEspline = electonsusceptibilitesdata.dataspline(1);
    data_array muonusceptibilitesdata("MuonSusTable.dat", 2);
    chiMspline = muonusceptibilitesdata.dataspline(1);
    data_array taususceptibilitesdata("TauSusTable.dat", 2);
    chiTspline = taususceptibilitesdata.dataspline(1);
    data_array neutrinosusceptibilitesdata("NeutrinoSusTable.dat", 2);
    chiNspline = neutrinosusceptibilitesdata.dataspline(1);

    // E_f factor relating rho = E_f * T^4 for g = 1 as function of m/T
    data_array EFdata("EFermi.dat", 2);
    EFspline = EFdata.dataspline(1);
    // evolution of chemical potentials as lepton asym evolves - flavour decided
    // by parameter file input
    if (flavour == 1) { // electron flavour asym
        data_array chempotentialdata_e("dmudLe.dat", 9);
        dmu_edL_x = chempotentialdata_e.dataspline(1);
        dmu_mudL_x = chempotentialdata_e.dataspline(2);
        dmu_tdL_x = chempotentialdata_e.dataspline(3);
        dmu_nuedL_x = chempotentialdata_e.dataspline(4);
        dmu_numudL_x = chempotentialdata_e.dataspline(5);
        dmu_nutdL_x = chempotentialdata_e.dataspline(6);
        dmu_QdL_x = chempotentialdata_e.dataspline(7);
        dmu_BdL_x = chempotentialdata_e.dataspline(8);
    } else if (flavour == 2) { // muon flavour asym
        data_array chempotentialdata_mu("dmudLmu.dat", 9);
        dmu_edL_x = chempotentialdata_mu.dataspline(1);
        dmu_mudL_x = chempotentialdata_mu.dataspline(2);
        dmu_tdL_x = chempotentialdata_mu.dataspline(3);
        dmu_nuedL_x = chempotentialdata_mu.dataspline(4);
        dmu_numudL_x = chempotentialdata_mu.dataspline(5);
        dmu_nutdL_x = chempotentialdata_mu.dataspline(6);
        dmu_QdL_x = chempotentialdata_mu.dataspline(7);
        dmu_BdL_x = chempotentialdata_mu.dataspline(8);
    } else if (flavour == 3) { // tau flavour asym
        data_array chempotentialdata_t("dmudLtau.dat", 9);
        dmu_edL_x = chempotentialdata_t.dataspline(1);
        dmu_mudL_x = chempotentialdata_t.dataspline(2);
        dmu_tdL_x = chempotentialdata_t.dataspline(3);
        dmu_nuedL_x = chempotentialdata_t.dataspline(4);
        dmu_numudL_x = chempotentialdata_t.dataspline(5);
        dmu_nutdL_x = chempotentialdata_t.dataspline(6);
        dmu_QdL_x = chempotentialdata_t.dataspline(7);
        dmu_BdL_x = chempotentialdata_t.dataspline(8);
    } else {
        std::cout << "Invalid flavour entered. Flavour must be 1,2 or 3 "
                     "corresponding to electron, muon or tau.";
        std::system("pause");
        std::exit(1);
    }

    // total active neutrino interaction rates (2D)
    if (method_rates == 1) { // Laine's rates data
        if (flavour == 1) {
            data_array totalratesdata("IQalpha1.txt");
            totalrates_x = totalratesdata.dataspline2D();
        } else if (flavour == 2) {
            data_array totalratesdata("IQalpha2.txt");
            totalrates_x = totalratesdata.dataspline2D();
        } else if (flavour == 3) {
            data_array totalratesdata("IQalpha3.txt");
            totalrates_x = totalratesdata.dataspline2D();
        } else {
            std::cout << "Invalid flavour entered. Flavour must be 1,2 or 3 "
                         "corresponding to electron, muon or tau.";
            std::system("pause");
            std::exit(1);
        }
    } else if (method_rates == 2) { // Abazajian's rates data
        if (flavour == 2) {
            data_array totalratesdata("rate_total_mu.dat", Tc, Tqcd);
            totalrates_x = totalratesdata.dataspline2D();
        } else {
            std::cout
                << "Invalid flavour entered. Only muon flavour rates data "
                   "available for Abazajian's rates.";
            std::system("pause");
            std::exit(1);
        }
    } else {
        std::cout << "Invalid rates data set. Valid inputs are 1 or 2, "
                     "corresponding to "
                     "Laine's or Abazajian's rates data respectively";
        std::system("pause");
        std::exit(1);
    }
}

// function to fill initial condition for integral
state_type equations::uinitial() {
    const double mpl = 1.2209101e22; // in MeV

    double gstar0 = gstarspline(Tb); // initial g*

    state_type res(3 + 2 * n);
    res[0] = (sqrt(45 / (4 * M_PI * M_PI * M_PI)) * mpl) /
             (sqrt(gstar0) * Tb * Tb); // initial t in MeV-1 (t = 1/H)
    res[1] = 0;                        // initial N = #efolds since start = 0
    res[2] = L0;                       // initial	L/T^3
    // fs, fsbar zero before production
    for (int i = 0; i < n; i++) {
        res[3 + i] = 0;
        res[3 + n + i] = 0;
    }
    return res;
}

/* Momentum grid:
 * Defined s.t. conformal momentum range:  klow < p/Tb < khigh
 * Set momenta to physical momenta at Tb, these are then scaled at each stage in
 * the integrator using number of efolds Log distributed bins
 */
dvector equations::pgrid() {
    // high and low momenta
    double phigh = khigh * Tb;
    double plow = klow * Tb;

    /*
    // linearly spaced grid
    dvector p(n);
    double dp = (phigh - plow) / (n - 1);
    for (int i = 0; i < n; i++) {
            p[i] = plow + i * dp;
    }
    return p;
    */

    // logrithmically spaced grid
    double dp = (log10(phigh) - log10(plow)) / (n - 1);
    dvector p(n);
    for (int i = 0; i < n; i++) {
        p[i] = pow(10, log10(plow) + (i)*dp);
    }
    return p;
}

double equations::trapz(state_type f, dvector p) {
    double sum = 0;
    for (size_t i = 0; i < p.size() - 1; i++) {
        sum += 0.5 * (f[i] + f[i + 1]) * (p[i + 1] - p[i]);
    }
    return sum;
}

// Set up integrating/sum function using real_2d_array
double equations::cubicsplineintegral(dvector f, dvector p) {
    tk::spline s;
    s.set_points(p, f);
    return s.integrate();
}

// Function to evaluate the dimensionless factor that relates the lepton
// asymmetry potential V_L to G_F*(L_X / T^3)*T^3, at temperature T (in MeV) for
// chosen asymmetry flavour
double equations::vasym(double T, int flavour) {
    // constants
    const double me = 0.511, mmu = 1.05658E2, mtau = 1.77682E3; // in MeV
    const double mnue = 0, mnumu = 0, mnutau = 0;
    const double s2w = 0.2223; // Weinberg angle

    // contributions of charged, neutral and strong currents
    double vlc = 0, vln = 0, vlq = 0;

    if (method_suspect == 2) { // Abazajan calculation
        // calculation flavour independent parts of vlc, vln, vlq
        vlc = sqrt(2) * (-0.5 + 2 * s2w) * 2 *
              (chiPspline((me / T)) * dmu_edL_x(T) +
               chiPspline((mmu / T)) * dmu_mudL_x(T) +
               chiPspline((mtau / T)) * dmu_tdL_x(T));

        vln = sqrt(2) * (chiPspline((mnue / T)) * dmu_nuedL_x(T) +
                         chiPspline((mnumu / T)) * dmu_numudL_x(T) +
                         chiPspline((mnutau / T)) * dmu_nutdL_x(T));

        vlq = sqrt(2) * (1 - 2 * s2w) *
              (chi2Qspline(T) * dmu_QdL_x(T) + chi11QBspline(T) * dmu_BdL_x(T));

        // add additional flavour dependant part
        if (flavour == 1) { // electron
            vlc = vlc + sqrt(2) * 2 * chiPspline((me / T)) * dmu_edL_x(T);
            vln = vln + sqrt(2) * chiPspline((mnue / T)) * dmu_nuedL_x(T);
        } else if (flavour == 2) { // muon
            vlc = vlc + sqrt(2) * 2 * chiPspline((mmu / T)) * dmu_mudL_x(T);
            vln = vln + sqrt(2) * chiPspline((mnumu / T)) * dmu_numudL_x(T);
        } else if (flavour == 3) { // tau
            vlc = vlc + sqrt(2) * 2 * chiPspline((mtau / T)) * dmu_tdL_x(T);
            vln = vln + sqrt(2) * chiPspline((mnutau / T)) * dmu_nutdL_x(T);
        }
    } else if (method_suspect == 1) {
        // Laine's code susceptibilities
        vlc = sqrt(2) * (-0.5 + 2 * s2w) * 2 *
              (chiEspline(T) * dmu_edL_x(T) + chiMspline(T) * dmu_mudL_x(T) +
               chiTspline(T) * dmu_tdL_x(T));

        vln = sqrt(2) * (chiEspline(T) * dmu_nuedL_x(T) +
                         chiMspline(T) * dmu_numudL_x(T) +
                         chiTspline(T) * dmu_nutdL_x(T));

        vlq = sqrt(2) * (1 - 2 * s2w) *
              (chi2Qspline(T) * dmu_QdL_x(T) + chi11QBspline(T) * dmu_BdL_x(T));

        // add additional flavour dependant part
        if (flavour == 1) { // electron
            vlc = vlc + sqrt(2) * 2 * chiEspline(T) * dmu_edL_x(T);
            vln = vln + sqrt(2) * chiNspline(T) * dmu_nuedL_x(T);
        } else if (flavour == 2) { // muon
            vlc = vlc + sqrt(2) * 2 * chiMspline(T) * dmu_mudL_x(T);
            vln = vln + sqrt(2) * chiNspline(T) * dmu_numudL_x(T);
        } else if (flavour == 3) { // tau
            vlc = vlc + sqrt(2) * 2 * chiTspline(T) * dmu_tdL_x(T);
            vln = vln + sqrt(2) * chiNspline(T) * dmu_nutdL_x(T);
        }
    }
    double vasym = vlc + vln + vlq;
    return vasym;
}

/* Derivatives function for odeint:
 * Variables are arranged as
 * Free variable:
 *  x -- log10(T)
 * State vector
 *  u[0]     - t
 *  u[1]     - N = # efolds since start = \int H dt
 *  u[2]     - L
 *  u[3+i]   - f[i],    i=0:n-1 -- sterile neutrino distribution functions for
 * each conformal mode u[3+n+i] - fbar[i], i=0:n-1 -- sterile antineutrino
 * distribution functions for each conformal mode
 */
void equations::operator()(const state_type &u, state_type &dudx,
                           const double x) {
    // constants
    const double GF = 1.1663787e-11;      // MeV^-2
    const double mZ2 = pow(91.1876e3, 2); // MeV^2
    const double mW2 = pow(80.385e3, 2);  // MeV^2
    const double mpl = 1.2209101e22;      // MeV
    const double me = 0.511, mmu = 1.05658E2,
                 mtau = 1.77682E3;                // lepton masses in MeV
    const double mnue = 0, mnumu = 0, mnutau = 0; // neutrino masses all set to zero

    // extract T, t, N, L
    double T = pow(10, x); // since x = log10(T)
    // double t = u[0];
    double N = u[1];
    double L = u[2];

    // calculate momentum scaling factor - defined as scalef = exp(-N) = a_start
    // / a_current
    double scalef = exp(-N);

    // get physical momentum array at this T step
    dvector grid = pgrid(); // initial physical momenta at Tb
    dvector p_array(n);
    for (int i = 0; i < n; i++) {
        p_array[i] = scalef * grid[i]; // scale to this temperature
    }

    // set mixing lepton mass, chemical potential - choice of flavour dependant
    double mx, mnux; // masses
    double munux;    // chemical potential mu/T
    if (flavour == 1) {
        mx = me;
        mnux = mnue;
        munux = dmu_nuedL_x(T) * L;
    } else if (flavour == 2) {
        mx = mmu;
        mnux = mnumu;
        munux = dmu_numudL_x(T) * L;
    } else if (flavour == 3) {
        mx = mtau;
        mnux = mnutau;
        munux = dmu_nutdL_x(T) * L;
    } else {
        std::cout << "Invalid flavour.";
        std::exit(1);
    }

    // calculate net energy of charged leptons and net energy of active
    // neutrinos
    double rhox = 4 * EFspline((mx / T)) * pow(T, 4);     // g = 2 + 2 (leptons)
    double rhonux = 2 * EFspline((mnux / T)) * pow(T, 4); // g = 2 (neutrinos)

    // vectors to store integrands calculated in loop of momentum bins
    // dLdt
    state_type dLdt_array(n);
    // rho_s and drho_s/dt
    state_type rhos_array(n);
    state_type drhosdt_array(n);

    // calculate asymmetry part of potential
    double VL = vasym(T, flavour) * GF * L * T * T * T;

    // calculate thermal potential factor relating VT to momentum p
    double vtherm = -(8 * sqrt(2) / 3) * GF * ((rhonux / mZ2) + (rhox / mW2));

    // printing values of potentials to a file for debugging purposes. Please
    // ignore std::ofstream myfile ("potential data.txt", std::ios_base::app);
    // myfile << "Temperature" << "      Asymmetry" << "    Thermal pot./p" << "
    // Asymmetry pot." << std::endl; myfile << std::setw(7) << pow(10, x) <<
    // std::setw(15) << L << std::setw(18) << vtherm << std::setw(18) << VL <<
    // std::endl; myfile.close();

    // -------------- evolution of momentum bins -------------
    for (int i = 0; i < n; i++) {
        // physical momentum
        double p = p_array[i];

        // active neutrino and antineutrino distribution functions
        double fk = 1 / (exp((p / T) - munux) + 1);
        double fbark = 1 / (exp((p / T) + munux) + 1);

        // active neutrino interaction rate
	double Fout = totalrates_x(T, p);
        double gamma = Fout * GF * GF * T * T * T * T * p;

        double D = gamma / 2; // damping factor
        double delta =
            (ms * ms) / (2 * p); // delta (oscillation rate parameter)
        double VT = vtherm * p;  // thermal part of potential

        double s2t = sin(2 * theta);
        double c2t = cos(2 * theta);

        // sterile neutrino df/dt and dfbardt
        // old original version

        double dfdt = 0, dfbardt = 0;
        if (method_eqn == 2) {
            dfdt = ((gamma * delta * delta * pow(s2t, 2) / 4) /
                    (delta * delta * pow(s2t, 2) + D * D +
                     pow(delta * c2t - VL - VT, 2))) *
                   (fk - u[3 + i]);
            dfbardt = ((gamma * delta * delta * pow(s2t, 2) / 4) /
                       (delta * delta * pow(s2t, 2) + D * D +
                        pow(delta * c2t + VL - VT, 2))) *
                      (fbark - u[3 + i + n]);
        } else if (method_eqn == 1) {
            // based on Laine's paper
            dfdt = ((gamma * delta * delta * pow(s2t, 2) / 4) /
                    (D * D + pow(delta * c2t - VL - VT +
                                     (-VL - VT) * (-VL - VT) / (2 * p),
                                 2))) *
                   (fk - u[3 + i]);
            dfbardt = ((gamma * delta * delta * pow(s2t, 2) / 4) /
                       (D * D + pow(delta * c2t + VL - VT +
                                        (VL - VT) * (VL - VT) / (2 * p),
                                    2))) *
                      (fbark - u[3 + i + n]);
        }

        // add to derivatives state vector
        dudx[3 + i] = dfdt;
        dudx[3 + n + i] = dfbardt;

        // build arrays of integrands for evolution of lepton asymmetry and
        // rho_sterile dL/dt - term due to sterile neutrino production:
        double dLdt_i =
            -(((p * p) / (2 * M_PI * M_PI * T * T * T)) * (dfdt - dfbardt));
        dLdt_array[i] = dLdt_i * 1e25;
        // rho_sterile integrands:
        double sterile_energy = sqrt(pow(p, 2) + pow(ms, 2)); // E^2 = p^2 + m^2
        double rhos_i = (1 / (2 * pow(M_PI, 2))) * pow(p, 2) * sterile_energy *
                        (u[3 + i] + u[3 + n + i]); // energy density
        rhos_array[i] = rhos_i;
        double drhosdt_i =
            (1 / (2 * pow(M_PI, 2))) * pow(p, 2) * sterile_energy *
            (dudx[3 + i] + dudx[3 + n + i]); // derivative of energy density
        drhosdt_array[i] = drhosdt_i;
    }
    // ----------------- background evolution -------------------
    // perform integrations over momenta

    // Calculate energy density of Sterile neutrinos, time evolution of energy
    // density of sterile neutrinos, dLdt --> time evolution of lepton assymetry
#ifndef TRAPEZOIDAL_L
    // Using cubic spline

    // Create table to hold energy density of sterile neutrinos

    double rho_s = cubicsplineintegral(rhos_array, p_array);
    // Create table to hold time derivative of energy density of sterile
    // neutrinos

    double drho_sdt = cubicsplineintegral(drhosdt_array, p_array);
    // Create table to hold dLdt

    double dLdt = cubicsplineintegral(dLdt_array, p_array) / 1e25;
//	std::cout << "cubic dLdt function" << dLdt << std::endl;
#else  // TRAPEZOIDAL_L
    double rho_s = trapz(rhos_array, p_array);
    double drho_sdt = trapz(drhosdt_array, p_array);
    double dLdt = trapz(dLdt_array, p_array) / 1e25;
#endif // TRAPEZOIDAL_L

    // calculate energy and entropy densities of SM particles
    double rhoSM = (pow(M_PI, 2) / 30) * gstarspline(T) * pow(T, 4);
    double entropySM = (2 * pow(M_PI, 2) / 45) * hstarspline(T) * pow(T, 3);

    // derivative of energy density of SM particles
    double dgstardT =
        gstarspline.derivative(T); // TODO: Never calculate derivatives of
                                   // gstar, should be taken from Laine!
    double drhoSMdT = (pow(M_PI, 2) / 30) *
                      (4 * gstarspline(T) * pow(T, 3) + dgstardT * pow(T, 4));

    // background
    // hubble rate
    double hubble_rate = sqrt(8 * M_PI / 3) * (1 / mpl) * sqrt(rhoSM + rho_s);
    // dNdt
    double dNdt = hubble_rate; // since N = \int H dt
    // dt/dT
    double dtdT = -drhoSMdT / (drho_sdt + 3 * hubble_rate * entropySM * T);

    // convert background derivatives from d/dt to d/dT
    double dNdT = dNdt * dtdT;
    double dLdT = dLdt * dtdT - 3 * (hubble_rate * dtdT + (1 / T)) *
                                    L; // adding in background term

    // set background equations derivatives
    dudx[0] = dtdT;
    dudx[1] = dNdT;
    dudx[2] = dLdT;

    // convert momentum bin derivatives from d/dt to d/dT
    for (int i = 0; i < n; i++) {
        dudx[3 + i] *= dtdT;
        dudx[3 + n + i] *= dtdT;
    }

    // fix base variable x to be log10(T) instead of T; i.e. derivatives w.r.t
    // log10(T)
    for (uint i = 0; i < dudx.size(); i++) {
        dudx[i] *= log(10) * T;
    }
}

double equations::abund(state_type u) {
    double rho_crit = 8.07E-35;   // in Mev^4
    const double Tcmb = 2.34E-10; // in MeV

    // calculate momenta
    // factor to scale momenta from Tb to Te
    double scalef = exp(-u[1]);
    // factor to scale momenta from Te to present day
    double conversion =
        (Tcmb / Te) * pow((hstarspline(Tcmb) / hstarspline(Te)), (1.0 / 3.0));
    // scale to present day momenta
    dvector pinitial = pgrid(); // recreate initial grid at Tb
    dvector p(n);
    for (int i = 0; i < n; i++) {
        p[i] = pinitial[i] * scalef * conversion; // and scale
    }

    // calculate integrands
    state_type abundIntegrand(n);
    state_type abundIntegrandBar(n);
    for (int i = 0; i < n; i++) {
        abundIntegrand[i] = (1 / (2 * M_PI * M_PI)) * pow(p[i], 2) *
                            sqrt(pow(p[i], 2) + pow(ms, 2)) * u[3 + i] * 1e30;
        abundIntegrandBar[i] = (1 / (2 * M_PI * M_PI)) * pow(p[i], 2) *
                               sqrt(pow(p[i], 2) + pow(ms, 2)) * u[3 + n + i] *
                               1e30;
    }

#ifndef TRAPEZOIDAL_L
    // Calculate integral using cubic spline
    // Create tables for abundSbar and abundS

    // Calculate integral using function -->cubicsplineintegral
    double abundS = cubicsplineintegral(abundIntegrand, p) / 1e30;
    double abundSbar = cubicsplineintegral(abundIntegrandBar, p) / 1e30;
    double Omega_h2 = (abundS + abundSbar) / rho_crit;

    std::cout << "omegah^2 cubic! ---->" << Omega_h2 << std::endl;
#else // TRAPEZOIDAL_L
      // Calculate abundance of sterile neutrinos and anti sterile neutrinos
      // using trapezium rule
    double abundS = trapz(abundIntegrand, p) / 1e30;
    double abundSbar = trapz(abundIntegrandBar, p) / 1e30;
    // Calculate abundance using trapezium rule
    //  abundance --> Using trapz instead of NIntegrate
    double OmegaS = abundS / rho_crit, OmegaSbar = abundSbar / rho_crit;
    double Omega_h2 = OmegaS + OmegaSbar;
#endif

    return Omega_h2;
}
