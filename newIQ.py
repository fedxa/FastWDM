
import numpy as np
import scipy as sp
from scipy import interpolate
import os
import matplotlib.pyplot as plt
import sys


path = '/Users/paulbebb/Desktop/MPhys/Project/Semester2/release/hatIQ_M0007_alpha3.dat'
data = np.genfromtxt(path,skip_header = 6)
allT = data[:,0]
allK = data[:,1]
IQ = data[:,2]

#create an array of index points at which the temperature changes use these to split the K data and IQ data
#so that we can make splines of the K and IQ data at each unique temperature.
split_points = []
for counter, value in enumerate(allT):
    if counter != len(allT)-1:
        if allT[counter + 1] !=value :
            split_points.append(counter)

T = []
T.append(allT[0])
for value in split_points:
    T.append(allT[value+1])


K_by_T = sp.array_split(allK,100)

IQ_by_T = sp.array_split(IQ,100)


new_K = np.linspace(0.06206,12.4688,200)

new_IQ = []

for i in range(0,100):
    f = interpolate.interp1d(K_by_T[i],IQ_by_T[i])
    temp = f(new_K)
    new_IQ.append(temp)

with open('IQalpha3.txt', 'w') as f:
    f.write('# Data interpolated from hep-ph/0612182 (sec.3) for alpha=1 \n' + '%15s' %'p/T,T->')
    for j in range(0,100):
        f.write('\t' + '%15s' %str(T[j]) )
    f.write('\n')
    for i in range(0,200):
        f.write( str(new_K[i]))
        for k in range(0,100):
            f.write('\t' + '%15s' %str(new_IQ[k][i]))
        f.write('\n')
            
    f.close()

