VERSION information
-------------------

* `MPhys2` branch: code from MPhys 2017, Mostly Abazajan, apart from availability of Laine's rate equations.

* `Tadas` branch: code from 2018 MPhys

        v1.1		changed f_active to f_active - f_sterile in sterile neturino production rate equation
        v1.2		updated production rate such that it matches Laine's Mathematica code
        v1.3		updated susceptibilities such that they match the ones calculated in Laine's Mathematica code
        v1.4		reverted the derivative of f_s back to old version
        V1.5 		added a minus sign in the asymmetry washout rate

* `master` branch: Combined both codes, some cleanup, equation/rates/suspectability are selectable


IMPORTANT INFORMATION REGARDING THE USE OF FWDM
-----------------------------------------------

Amongst the resource files is a file named params.json and this file is used
to set the paramaters for the calculations. Below I list each parameter that
can be set in the .json file and give a brief description of each.


1) "ms" :
ms denotes the mass of the sterile neutrino and it takes units of MeV
e.g. to run the code with ms = 7.1 keV you would set

"ms" : 0.0071

2) "theta" :
theta denotes the mixing angle between the active flavour basis (that was chosen to 
have non-zero mixing angle) and the sterile basis. it is the actual angle and NOT
sin^2(2theta), which is often just called 'the mixing angle'.

3) "L0" :
L0 is the initital lepton asymmetry normalised by T^3 i.e L0 = (L)/Tb^3 where Tb is the
starting temperature and L is the asymmetry in the neutrinos of the chosen flavour +
the asymmetry in the charged leptons of the chosen flavour.

4) "flavour" : 
Flavour of mixing + asym can be set to either 1,2 or 3. 
	1 -> electron
	2 -> muon
	3 -> tauon

5) "Tb":
Tb denotes the starting temperature for the integrator in MeV.

6) "Te":
Te denotes the last temperature for the integrator in MeV. 

7) "khigh" :
khigh denotes the largest initial conformal mode p/T. 

8) "klow"
klow denotes the smallest initial conformal mode p/T

9) "n" :
n = the total number of momentum bins

10) "abs_err" :
abs_error is the absolute error for the integrator error stepper

11) "rel_err" :
rel_err is the relative error for the integrator error stepper

12) "mode_coeff" :
mode_coeff is the coefficient that weights the error on the distribution functions calculated for each momentum mode, forcing the adaptive stepper to take smaller steps

13) "derivs" :
derivs takes a string e.g. "filename.out" for naming the output file that contains the derivatives
data. if nothing is entered the default is "derivs.out"

14) "distribs" :
distribs takes a string e.g. "filename.out" for naming the output file that contains the distribuition data. if nothing is entered the default is "distribs.out"

15) "Tc" :
Tc sets the high cutoff point for abazajians rate data.

16) "Tqcd" :
Tqcd sets the low cut off point for abazajians rate data. 
(i.e the rate data between Tqcd and Tc is removed) 

17) "method.rates" (was rates.dataset before):
Sets which active neutrino opacity data set to use:
	1 -> Laine's data : valid for all flavours
	2 -> Abazajians's data : only valid for muon flavour (flavour = 2)

18) "method.suspectabilities" (was rates.dataset before):
Sets which suspectability calculation (and, respectively, VL potential)
	1 -> Laine's data : valid for all flavours
	2 -> Abazajians's data

19) "method.equations" (was rates.dataset before):
Sets which style of the resonance production equations to use
	1 -> Laine's, based on density matrix approach originally
	2 -> Abazajians's
