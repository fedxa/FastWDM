#include "data_array.hpp"

#ifdef USE_BOOST_FILESYSTEM_PATH
#include <boost/filesystem.hpp>
#include <boost/filesystem/fstream.hpp>

typedef boost::filesystem::path path;
typedef boost::filesystem::ifstream ifstream;
#else
typedef std::ifstream ifstream;
#endif

// function to check file can be opened
#ifdef USE_BOOST_FILESYSTEM_PATH
inline bool validfile(path fname) {
#else
inline bool validfile(const std::string &fname) {
#endif
    ifstream f(fname);
    if (!f.good()) {
        std::cout << "Error: file " << fname << " could not be opened.";
        exit(1);
    }
    return true;
}

// function to read 1D data files for 1D splines
// files all have two header lines, then space deliminated elements of data on
// each line
void data_array::readfile(std::string fname,
                          int elements) { // "elements" is number of columns
    // create dvectors to store each type of data in and add each vector to
    // overall data vector
    for (int i = 0; i < elements; i++) {
        dvector xi;
        data.push_back(xi);
    }
#ifdef USE_BOOST_FILESYSTEM_PATH
    path path = "datafiles";
    path /= fname;
    // test if file opens
#else
    std::string path = "datafiles/";
    path = path + fname;
#endif
    validfile(path);
    // open data file
    ifstream datafile(path);
    // read data into dvectors
    int linecounter = 0;
    while (!datafile.eof()) {
        std::string line;
        getline(datafile, line);
        if (linecounter == 0 || linecounter == 1) {
        } // skip header lines
        else {
            // split line into elements and add to appropriate vectors
            std::stringstream ssplit{line};
            double data_element;
            int entrycounter{0};
            while (ssplit >> data_element && entrycounter < elements) {
                data[entrycounter].push_back(data_element);
                entrycounter++;
            }
        }
        linecounter++;
    }
    // close data file
    datafile.close();
}

// function to read 2D interaction rates data files in order to make 2D spline
// for Abazajian's rates data Abazajian's data requires scaling each rate by
// dividing by p/T + cutting out data between Tc and Tqcd file one header line,
// then space deliminated elements of data on each line - first line = T, first
// column = p/T
void data_array::readfile2DAbazajian(std::string fname, double Tc,
                                     double Tqcd) {
    // create dvectors to store each type of data in and add each vector to
    // overall data vector: p/T, T, f(p/T,T)
    dvector x1;
    dvector x2;
    dvector x3;
    data.push_back(x1);
    data.push_back(x2);
    data.push_back(x3);

#ifdef USE_BOOST_FILESYSTEM_PATH
    path path = "datafiles";
    path /= fname;
    // test if file opens
#else
    std::string path = "datafiles/";
    path = path + fname;
#endif
    validfile(path);
    // open data file
    ifstream datafile(path);
    // read data into dvectors
    int linecounter = 0;
    int highnum{0};
    int lownum{0};
    int removednum{
        0}; // number of colummns above / below cutoff and number removed
    while (!datafile.eof()) {
        std::string line;
        getline(datafile, line);
        if (linecounter == 0) {
	    // skip header line
        } else if (linecounter == 1) { // first line is T -> stick in data[0]
            // split line into elements and add to appropriate vectors
            std::stringstream ss{line};
            std::string temp;
            double data_element;
            int entrycounter{0};
            while (ss >> temp) {
                if (entrycounter == 0 || entrycounter == 1 ||
                    entrycounter == 2) {
		    // first,second,third element is description
                } else if (stod(temp) <= Tqcd) {
                    lownum++;
                    data_element = std::stod(temp);
                    data[0].push_back(data_element);
                } else if (stod(temp) >= Tc) {
                    highnum++;
                    data_element = std::stod(temp);
                    data[0].push_back(data_element);
                } else {
                    removednum++;
                }
                entrycounter++;
            }
        } else { // subsequent lines: first element is p/T -> stick in data[1],
                 // other elements are f(T,p/T) all go in data[2] for 2D spline
            // split line into elements and add to appropriate vectors
            std::istringstream ss(line);
            double data_element;
            double scaling;
            int entrycounter = 0;
            while (ss >> data_element) {
                if (entrycounter == 0) { // p/T -> data[1]
                    data[1].push_back(data_element);
                    scaling = data_element;
                } else if (entrycounter <= lownum ||
                           entrycounter >
                               lownum + removednum) { // rate data, excluding
                                                      // between cutoff Temps
		    // TODO Check scaling in th eoriginal Abazajan's code
                    double scaled_data_element =
                        data_element / scaling; // divide by p/T
                    data[2].push_back(scaled_data_element);
                }
                entrycounter++;
            }
        }
        linecounter++;
    }
    datafile.close();
}


// function to read 2D interaction rates data files in order to make 2D spline
// for Laine's rates data (hatIQ files) file one header line, then space
// deliminated elements of data on each line - first line = T, first column =
// p/T
void data_array::readfile2DLaine(std::string fname) {
    // create dvectors to store each type of data in and add each vector to
    // overall data vector: p/T, T, f(p/T,T)
    dvector x1;
    dvector x2;
    dvector x3;
    data.push_back(x1);
    data.push_back(x2);
    data.push_back(x3);

#ifdef USE_BOOST_FILESYSTEM_PATH
    path path = "datafiles";
    path /= fname;
#else
    std::string path = "datafiles/";
    path = path + fname;
#endif
    // test if file opens
    validfile(path);
    // open data file
    ifstream datafile(path);
    // read data into dvectors
    int linecounter = 0;
    while (!datafile.eof()) {
        std::string line;
        getline(datafile, line);
        if (linecounter == 0) {
        }                            // skip header line
        else if (linecounter == 1) { // first line is T -> stick in data[0]
            // split line into elements and add to appropriate vectors
            std::stringstream ss{line};
            std::string temp;
            double data_element;
            int entrycounter{0};
            while (ss >> temp) {
                if (entrycounter == 0 || entrycounter == 1 ||
                    entrycounter == 2) {
                } // first,second,third element is description
                else {
                    data_element = std::stod(temp);
                    data[0].push_back(data_element);
                }
                entrycounter++;
            }
        } else { // subsequent lines: first element is p/T -> stick in data[1],
                 // other elements are f(T,p/T) all go in data[2] for 2D spline
            // split line into elements and add to appropriate vectors
            std::stringstream ss{line};
            double data_element;
            int entrycounter{0};
            while (ss >> data_element) {
                if (entrycounter == 0) { // p/T -> data[1]
                    data[1].push_back(data_element);
                } else {
                    data[2].push_back(data_element);
                }
                entrycounter++;
            }
        }
        linecounter++;
    }
    // close data file
    datafile.close();
}

// function to create spline of data[i] as function of data[0] (temperature)
// ---- linear 1D spline
tk::spline data_array::dataspline(int i) {
    // extract arrays of data elements for use in spline
    std::vector<double> AX = data[0]; // T - always data[0]
    std::vector<double> AY = data[i];
    // build spline
    tk::spline spline;
    spline.set_points(AX, AY);
    return spline;
}

// function to create 2D spline of data[2] = f(T,p/T) where T = data[0] and p/T
// = data[1] ---- bicubic 2D spline
class gamfac_spline data_array::dataspline2D() {
    return gamfac_spline(data[0], data[1], data[2]);
}






gamfac_spline::gamfac_spline(std::vector<double>& Tvec, std::vector<double>& pTvec,
			     std::vector<double>& gamma) {
    // extract arrays of data elements for use in alglib 2D spline
    alglib::real_1d_array AX, AY, F;

    // Allow for a bit of extrapolation to get edges valid
    Tmin = *std::min_element(Tvec.begin(), Tvec.end()) * 0.99;
    Tmax = *std::max_element(Tvec.begin(), Tvec.end()) * 1.01;
    AX.setcontent(Tvec.size(), Tvec.data());
    for (int i=0; i<AX.length(); i++) {
	AX[i] = log(AX[i]); // Interpolation is in log(T)
    }

    pTmin = *std::min_element(pTvec.begin(), pTvec.end()) * 0.99;
    pTmax = *std::max_element(pTvec.begin(), pTvec.end()) * 1.01;
    AY.setcontent(pTvec.size(), pTvec.data()); // p/T
    for (int i=0; i<AY.length(); i++) {
	AY[i] = log(AY[i]); // Interpolation is in log(p/T)
    }

    F.setcontent(gamma.size(), gamma.data());  // f(T,p/T)
    for (int i=0; i<F.length(); i++) {
	F[i] = log(F[i]); // Interpolation was of log(gamma)
	// I woudl say this is rather irrelevant
    }

    // build spline
    alglib::spline2dbuildbicubicv(AX, AX.length(), AY, AY.length(), F, 1,
				  spline2d);
}
