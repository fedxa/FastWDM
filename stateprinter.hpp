/* Class that saves the current state line by line:
 * Writes states and derivatives to separate files for every step of the
 * integrator Also outputs current temperature to console
 */
#ifndef __STATEPRINTER_HPP
#define __STATEPRINTER_HPP

#include "equations.hpp"
#include <iostream>

class stateprinter {
    int step;
    equations system;
    std::ostream &os1;
    std::ostream &os2;
    std::ostream &os3;

public:
    // Constructors
    // stateprinter(equations& s) : step(0), system(s), os1(NULL), os2(NULL),
    // os3(NULL) { }
    stateprinter(equations &s, std::ostream &distribs, std::ostream &derivs,
                 std::ostream &maxderivsout)
        : step(0), system(s), os1(distribs), os2(derivs), os3(maxderivsout) {}

    // Print out the values at each integrator step
    void operator()(const state_type &u, const double x) {
        // IF we want to print out the derivatives
        // Fill the equations This is an extra calculation at each
        // step, but there are anyway about 4 or 5 per step for RK4
        state_type du(u.size());
        system(u, du, x);
        dvector grid = system.pgrid(); // initial physical momenta at Tb

        double mdu = abs(du[3]);
        size_t maxbin = 3;
        for (size_t i = 4; i < du.size(); i++) {
            if (abs(du[i]) > mdu) {
                mdu = abs(du[i]);
                maxbin = i;
            };
        }

        step++;

        std::cerr << step << "\t" << pow(10, x) << "\r";
        os3 << step << "\t" << pow(10, x) << "\t";
        os3 << maxbin << "\t" << grid[(maxbin - 3) % system.n] << "\t" << mdu
            << "\t";
        os3 << std::endl;
        if (x == 1) {

            state_type k = system.pgrid();

            static bool header(true);
            // Header lines
            if (header == true) {
                os1 << "Values in the header row after the four column headers "
                       "are conformal momenta. The values beneth them are "
                       "corresponding f(p)\n";
                // os2 << "Values in the header row after the four column
                // headers are conformal momenta. The values beneth them are
                // corresponding df(p)/dp\n";

                os1 << "      Temp(MeV)\t"
                    << "        Time(MeV-1)\t"
                    << "              N\t"
                    << "      Lasym/T^3";
                // os2 << "      Temp(MeV)\t" << "        Time(MeV-1)\t" << "
                // N\t" << "      Lasym/T^3";

                for (auto iterate = k.begin(); iterate != k.end(); iterate++) {
                    os1 << "\t" << std::setw(15) << std::right << *iterate;
                    // os2 << "\t" << std::setw(15) << std::right << *iterate;
                }
                for (auto iterate = k.begin(); iterate != k.end(); iterate++) {
                    os1 << "\t" << std::setw(15) << std::right << *iterate;
                    // os2 << "\t" << std::setw(15) << std::right << *iterate;
                }
                os1 << "\n";
                // os2 << "\n";
                header = false;
            }
            for (uint i = 0; i < u.size(); i++) {
                if (i == 0) {
                    os1 << std::setw(15) << pow(10, x) << "\t";
                }
                if (i == u.size() - 1) {
                    os1 << std::setw(15) << std::right << u[i] << "\n";
                } else {
                    os1 << std::setw(15) << std::right << u[i] << "\t";
                }
            }

            // for (uint i = 0; i<du.size(); i++) {
            //	if (i == 0) { os2 << std::setw(15) << pow(10, x) << "\t"; }
            //	if (i == du.size() - 1) { os2 << std::setw(15) << std::right <<
            //du[i] << "\n"; } 	else { os2 << std::setw(15) << std::right <<
            //du[i] << "\t"; }
            // }
        }
    }
};

#endif
