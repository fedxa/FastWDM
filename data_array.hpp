/* Class to read in data from data files and store in data array. */
#ifndef __DATA_ARRAY_HPP__
#define __DATA_ARRAY_HPP__

#include "config.hpp"
#include <fstream>
#include <iostream>
#include <sstream>
#include <string>
#include <vector>

#include "alglib/interpolation.h" // alglib interpolation for splines -- make sure to have alglib in source files
#include "spline.h"

typedef std::vector<double> dvector; // vector of doubles

// class to read in and store data
class data_array {
private:
    std::vector<std::vector<double>> data;

public:
    // parameterised constructors
    data_array(std::string fname, int elements) {
        readfile(fname, elements);
    }; // 1D files elements == number columns
    data_array(std::string fname) {
        readfile2DLaine(fname);
    }; // 2D files - Laine
    data_array(std::string fname, double Tc, double Tqcd) {
        readfile2DAbazajian(fname, Tc, Tqcd);
    }

    void readfile(std::string fname, int elements); // read 1D data files
    void readfile2DLaine(std::string fname);        // read Laine 2D rates files
    void readfile2DAbazajian(std::string fname, double Tc,
                             double Tqcd); // read Abazajians 2D rates files

    // double output(int i, int j) { return data[i][j]; } // output data[i][j]

    // function to create spline of data[i] as function of data[0] = T ---- 1D
    // spline
    tk::spline dataspline(int i);
    // function to create 2D spline of data[2] = f(T,p/T) where T = data[0] and
    // p/T = data[1] ---- 2D spline
    class gamfac_spline dataspline2D();
};


/*
 * The class to store and then calculate the dimensionless factor gamma
 * in the neutrino opacities
 * Gamma = gamma GF^2 T^4 p 
 */
class gamfac_spline {
private:
    alglib::spline2dinterpolant spline2d;
    double Tmin, Tmax;
    double pTmin, pTmax;
    
public:
    // Invalid spline by default
    gamfac_spline(): Tmin(0), Tmax(-1), pTmin(0), pTmax(0) {}
    
    gamfac_spline(std::vector<double>& Tvec, std::vector<double>& pTvec,
		  std::vector<double>& gamma);

    double operator() (double T, double p) {
	if (T<Tmin || T>Tmax) {
	    std::cerr << "gamfac evaluated for T=" << T
		      << " outside range " << Tmin << "," << Tmax << "\n";
	    exit(1);
	}
	double pT = p/T;
	if (pT<pTmin || pT>pTmax) {
	    std::cerr << "gamfac evaluated for p/T=" << pT
		      << " outside range " << pTmin << "," << pTmax << "\n";
	    exit(1);
	}
	return exp(
	    alglib::spline2dcalcvi(spline2d, log(T), log(pT), 0)
	    );
    }
};


#endif
