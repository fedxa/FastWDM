import matplotlib.pyplot as plt

filename = "SMgstar.dat"

T = []
g = []
gs = []

ifile = open(filename, 'r')
ifile.readline()
ifile.readline()

for line in ifile:
    splitted = line.split()
    T.append(float(splitted[0]))
    g.append(float(splitted[1]))
    gs.append(float(splitted[2]))
ifile.close()

plt.figure(1, figsize = (10,6))
plt.plot(T, g, "r")
plt.plot(T, gs, "r--")
plt.xlabel("$T$, MeV", fontsize = 20)
plt.ylabel("$g^*$", fontsize = 20)
plt.xticks(fontsize = 15)
plt.yticks(fontsize = 15)
#plt.ylim(1, 250)

ofile = open("SMgstar2.dat", 'w')

line1 = "#line1"
line2 = "#                 T(MeV)                     g_*                   g_*,s"

ofile.write(line1 + "\n")
ofile.write(line2 + "\n")
for i in range(len(T)):
    ofile.write("%.16e %.16e %.16e \n" % (T[i], 10.75, 10.75))
ofile.close()