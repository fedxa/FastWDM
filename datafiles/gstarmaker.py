import matplotlib.pyplot as plt

filename = "dmudLmu.dat"

T = []
dmu_edL_x = []
dmu_mudL_x = []
dmu_tdL_x = []
dmu_nuedL_x = []
dmu_numudL_x = []
dmu_nutdL_x = []
dmu_QdL_x = []
dmu_BdL_x = []

ifile = open(filename, 'r')
ifile.readline()
ifile.readline()

for line in ifile:
    splitted = line.split()
    T.append(float(splitted[0]))
    dmu_edL_x.append(float(splitted[1]))
    dmu_mudL_x.append(float(splitted[2]))
    dmu_tdL_x.append(float(splitted[3]))
    dmu_nuedL_x.append(float(splitted[4]))
    dmu_numudL_x.append(float(splitted[5]))
    dmu_nutdL_x.append(float(splitted[6]))
    dmu_QdL_x.append(float(splitted[7]))
    dmu_BdL_x.append(float(splitted[8]))
ifile.close()

plt.figure(1, figsize = (10,6))
plt.semilogx(T, dmu_edL_x, "r", label = "dmu_edL_x")
plt.semilogx(T, dmu_mudL_x, "b", label = "dmu_mudL_x")
plt.semilogx(T, dmu_tdL_x, "g", label = "dmu_tdL_x")
plt.semilogx(T, dmu_nuedL_x, "y", label = "dmu_nuedL_x")
plt.semilogx(T, dmu_numudL_x, "k", label = "dmu_numudL_x")
plt.semilogx(T, dmu_nutdL_x, "orange", label = "dmu_nutdL_x")
plt.semilogx(T, dmu_QdL_x, "purple", label = "dmu_QdL_x")
plt.semilogx(T, dmu_BdL_x, "violet", label = "dmu_BdL_x")
plt.title("evolution of chemical potentials as lepton asym evolves", fontsize = 20)
plt.legend(prop={'size': 10})
plt.xlabel("$m/T$", fontsize = 20)
#plt.ylabel("$E_f$", fontsize = 20)
plt.xticks(fontsize = 15)
plt.yticks(fontsize = 15)
#plt.ylim(1, 250)