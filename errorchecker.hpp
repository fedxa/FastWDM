/*
 * Custom error checker -  allows error estimates on evolved modes to be altered
 * without changing estimates for the background evolution equations (eqns
 * 0,1,2). Adapted from boost/numeric/odeint/stepper/controlled_runge_kutta.hpp
 * (by Fedor)
 */
#ifndef __ERRORCHECKER_HPP
#define __ERRORCHECKER_HPP

#include <boost/numeric/odeint.hpp>
#include <cmath>

namespace odi = boost::numeric::odeint;

typedef double value_type;
typedef odi::range_algebra algebra_type;
typedef odi::default_operations operations_type;

class errorchecker {
private:
    value_type m_eps_abs;
    value_type m_eps_rel;
    value_type m_a_x;
    value_type m_a_dxdt;
    value_type m_mode_coeff;

public:
    // Constructor -- fills the default parameters as in odeint, and also the
    // MODE_COEFF that will be used to scale errors for mode equations
    errorchecker(value_type eps_abs = 1e-6, value_type eps_rel = 1e-6,
                 value_type a_x = 1, value_type a_dxdt = 1,
                 value_type mode_coeff = 1)
        : m_eps_abs(eps_abs), m_eps_rel(eps_rel), m_a_x(a_x), m_a_dxdt(a_dxdt),
          m_mode_coeff(mode_coeff) {}

    // calculate the errors
    template <class State, class Deriv, class Err, class Time>
    value_type error(algebra_type &algebra, const State &x_old,
                     const Deriv &dxdt_old, Err &xerr, Time dt) const {
        // Enhance error for the modes, as compared to the background
        for (unsigned int i = 3; i < xerr.size(); i++) {
            xerr[i] *= m_mode_coeff;
        }
        using std::abs;
        // this overwrites x_err !
        algebra.for_each3(
            xerr, x_old, dxdt_old,
            typename operations_type::template rel_error<value_type>(
                m_eps_abs, m_eps_rel, m_a_x,
                m_a_dxdt * abs(odi::get_unit_value(dt))));

        /* The previous magic form odeint
        completely equivalently translated to the language of mortals */
        // for (uint i=0; i<xerr.size(); i++) {
        //     xerr[i] /= m_eps_abs+m_eps_rel*(
        // 	m_a_x*abs(x_old[i])+m_a_dxdt*abs(dxdt_old[i]) );
        // }

        // Return the maximum error -- this is just max(xerr)
        return algebra.norm_inf(xerr);
    }
};

#endif
