set yrange [0:20]

plot "< build/printout_rates 4000" us 1:2, "" us 1:3
replot "< build/printout_rates 1000" us 1:2, "" us 1:3
replot "< build/printout_rates 100" us 1:2, "" us 1:3
replot "< build/printout_rates 10" us 1:2, "" us 1:3
