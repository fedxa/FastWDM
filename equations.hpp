/* Definitions for equations governing sterile neutrino production */
#ifndef __EQUATIONS_HPP
#define __EQUATIONS_HPP
// This one is only for ancient compilers?
// #define _USE_MATH_DEFINES

#include "config.hpp"

#include <cmath>
#include <iostream>
#include <string>
#include <vector>

// This is required by odeint on unpachted boost 1.64.0
#include <boost/numeric/odeint.hpp>
#include <boost/serialization/array_wrapper.hpp>

#include <boost/property_tree/json_parser.hpp>
#include <boost/property_tree/ptree.hpp>

#include "spline.h"

#include "data_array.hpp"

typedef std::vector<double> dvector; // vector of doubles
typedef dvector state_type;          // system states
typedef unsigned int uint;           // unsigned integer

/* Class calculating the Boltzmann equation for sterile neutrino production --
 * integrand for odeint */
class equations {
public:
    double ms, theta; // sterile neutrino parameters [ms] = MeV
    int flavour; // mixing & asymmetry flavour: e = 1, mu = 2, tau = 3 - assume
                 // only mixing from single active flavour
    int method_rates;   // switch for rates data set to use: Laines data = 1,
                        // Abazajian's data = 2
    int method_suspect; // Laine suspectabilities = 1, Abazajan = 2
    int method_eqn;     // Laine equations = 1, Abazajan equations = 2
    double L0, Tb, Te;  // initial values / integration boundaries [T] = MeV
    double klow, khigh;
    int n;           // momentum grid parameters
    double Tc, Tqcd; // cutoff values for Abazajian rates data

    // splines of input data - generated by buildsplines() function :
    tk::spline gstarspline,
        hstarspline;       // SM gstar and hstar [Function of T(MeV)]
    tk::spline chiPspline; // particle number susceptibilities [Function of m/T]
    tk::spline chiEspline, chiMspline, chiTspline,
        chiNspline; // Laine's susceptibilities for electron, muon, tau and
                    // neutrinos
    tk::spline chi2Bspline, chi2Qspline,
        chi11QBspline; // strong fluid susceptibilities [Function of T(MeV)]
    tk::spline
        EFspline; // E_f factor: rho = E_f * T^4 for g = 1 [Function of m/T]
    // rates of change of chemical potential with lepton asymmetry
    // x = electron, muon or tau (flavour = 1, 2 or 3) as chosen in params file
    tk::spline dmu_edL_x, dmu_mudL_x,
        dmu_tdL_x; // electron, muon, tau chemical potentials
    tk::spline dmu_nuedL_x, dmu_numudL_x,
        dmu_nutdL_x; // electron, muon, tau neutrino chemical potentials
    tk::spline dmu_QdL_x,
        dmu_BdL_x; // charge and baryon number chemical potentials
    // active neutrino interaction rates
    alglib::spline2dinterpolant
        totalratesspline; // total muon neutrino interaction width 2D spline
                          // [f(T,p/T)] in MeV
    gamfac_spline totalrates_x; // total neutrino interation width
                                // 2D spline f(T,p/T) in MeV

    // public:
    //  default constructor
    void init_default_params() {
        ms = 0;
        theta = 0;
        flavour = 2;
        method_rates = 1;
        method_suspect = 1;
        method_eqn = 2;
        L0 = 0;
        Tb = 0;
        Te = 0;
        klow = 5e-3;
        khigh = 10;
        n = 100;
        Tc = 1000;
        Tqcd = 178;
    }
    /* initialise sterile neutrino and integration properties from property tree for
     * use in constructor */
    void initialise(const boost::property_tree::ptree &tree) {
	ms = tree.get<double>("sterile.ms", ms);
	theta = tree.get<double>("sterile.theta", theta);
	flavour = tree.get<int>("flavour", flavour);
	method_rates = tree.get<int>("method.rates", method_rates);
	method_suspect = tree.get<int>("method.suspectabilities", method_suspect);
	method_eqn = tree.get<int>("method.equations", method_eqn);
	L0 = tree.get<double>("L0", L0);
	Tb = tree.get<double>("Tb", Tb);
	Te = tree.get<double>("Te", Te);
	klow = tree.get<double>("grid.klow", klow);
	khigh = tree.get<double>("grid.khigh", khigh);
	n = tree.get<int>("grid.n", n);
	Tc = tree.get<double>("cutoff.Tc", Tc);
	Tqcd = tree.get<double>("cutoff.Tqcd", Tqcd);
    }

    // equations() { init_default_params(); }
    // parameterised constructor - reads from params.json
    equations(const boost::property_tree::ptree &tree) {
        init_default_params();
        initialise(tree);
        buildsplines();
    }

    void buildsplines(); // initialise data splines function

    state_type uinitial(); // set initial state function
    dvector pgrid();       // set momentum grid function

    // Integrators for L equations
    // alglib::real_2d_array cubic_table(state_type f, dvector x);
    double cubicsplineintegral(state_type f, dvector p);
    alglib::real_2d_array cubic_table(state_type f, dvector x);
    double trapz(state_type f, dvector p);

    double
    vasym(double T,
          int flavour); // function to calculate dimensionless vasym factor

    // main function for odeint - generates derivatives of given vector u and
    // "time" x
    void operator()(const state_type &u, state_type &dudx, const double x);

    // function to calculate final abundance
    double abund(state_type u);
};

#endif
