#ifndef __CONFIG_HPP__
#define __CONFIG_HPP__

#define _USE_MATH_DEFINES
#include <cmath>

// whther to use portable path names from boost -- useless
#undef USE_BOOST_FILESYSTEM_PATH

#endif
